var express = require('express');
var ejs = require('ejs');
var app = express();

app.set('views', `${__dirname}/dist`);
app.engine('html', ejs.renderFile);
app.set('view engine','html');

app.use(express.static(__dirname + '/dist'));

// app.use('/', (req, res) => {
//   res.render('/front/index.html', {});
// });

// app.use("/common", express.static(__dirname + '/common'));
// app.use("/css", express.static(__dirname + '/css'));



var server = app.listen(8001, () =>{
  console.log('done');
});